import { Router } from 'express'
import { UserRouter } from '../../modules/Users/routes/user.route'

export const router = Router()

router.use('/api', UserRouter)
