import { DataSource } from 'typeorm'
import { User } from './entities/User'
import { UserToken } from './entities/UserToken'
import { CreateUser1657101689653 } from './migrations/1657101689653-CreateUser'
import { CreateUserTokens1657150924432 } from './migrations/1657150924432-CreateUserTokens'

export const database = new DataSource({
	type: 'postgres',
	host: 'localhost',
	port: 5432,
	username: 'postgres',
	password: 'admin',
	database: 'postgres',
	synchronize: true,
	migrations: [CreateUser1657101689653, CreateUserTokens1657150924432],
	entities: [User, UserToken]
})
