import { NextFunction, Request, Response } from 'express'
import { verify } from 'jsonwebtoken'
import { InternalAppError } from '../errors/internalAppError'

export const isAuthenticated = (req: Request, _res: Response, next: NextFunction): void => {
	try {
		const authHeader = req.headers.authorization

		if (authHeader === undefined) {
			throw new InternalAppError('JWT Token is missing.')
		}

		const [, token] = authHeader.split(' ')

		const decodedToken = verify(token, process.env.JWT_SECRET as string)
		const { sub } = decodedToken

		req.user = {
			id: sub as string
		}

		return next()
	} catch {
		throw new InternalAppError('Invalid JWT Token.')
	}
}
