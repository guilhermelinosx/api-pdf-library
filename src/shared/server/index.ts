import { database } from '../database'
import { app } from './app'

database
	.initialize()
	.then(() => {
		app.listen(process.env.PORT || 3001, () => {
			console.info(`Server listening on: http://localhost:${process.env.PORT as string} 🚀`)
		})
	})
	.catch(err => {
		console.info(err)
	})
