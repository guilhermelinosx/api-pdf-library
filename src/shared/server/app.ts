/* eslint-disable @typescript-eslint/no-unused-vars */
import 'express-async-errors'
import 'reflect-metadata'
import cors from 'cors'
import { errors } from 'celebrate'
import { router } from '../router'
import express, { NextFunction, Request, Response } from 'express'
import { InternalAppError } from '../utils/errors/internalAppError'

export const app = express()

app.use(express.json())
app.use(router)
app.use(
	cors({
		origin: '*'
	})
)

app.use(errors())
app.use((err: Error, _req: Request, res: Response, _next: NextFunction) => {
	if (err instanceof InternalAppError) {
		return res.status(400).json({
			status: 'Error',
			message: err.message
		})
	}

	return res.status(500).json({
		status: 'Error',
		message: 'Internal server error'
	})
})
