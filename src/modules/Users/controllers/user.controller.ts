import { Request, Response } from 'express'
import { ForgotPasswordService } from '../services/ForgotPasswordService'
import { ResetPasswordService } from '../services/ResetPasswordService'
import { SignInUserService } from '../services/SignInUserService'
import { SignUpUserService } from '../services/SignUpUserService'

export class UserController {
	public async signup(req: Request, res: Response): Promise<Response> {
		try {
			const { name, email, password } = req.body
			const signUpUser = new SignUpUserService()
			await signUpUser.execute({ name, email, password })
			return res.status(201).json()
		} catch (err) {
			return res.status(400).json(err)
		}
	}

	public async signin(req: Request, res: Response): Promise<Response> {
		try {
			const { email, password } = req.body
			const signInUser = new SignInUserService()
			await signInUser.execute({ email, password })
			return res.status(200).json()
		} catch (err) {
			return res.status(400).json(err)
		}
	}
	public async forgotPassword(req: Request, res: Response): Promise<Response> {
		try {
			const { email } = req.body
			const forgotPasswordService = new ForgotPasswordService()
			const user = await forgotPasswordService.execute({ email })
			return res.status(200).json(user)
		} catch (err) {
			return res.status(400).json(err)
		}
	}

	public async resetPassword(req: Request, res: Response): Promise<Response> {
		try {
			const { token, password } = req.body
			const resetPasswordService = new ResetPasswordService()
			await resetPasswordService.execute({ token, password })
			return res.status(200).json({})
		} catch (err) {
			return res.status(400).json(err)
		}
	}
}
