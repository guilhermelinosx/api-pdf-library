import { compare } from 'bcryptjs'
import { sign } from 'jsonwebtoken'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { IUser } from '../models/IUser'
import { UserRepository } from '../repositories/user.repository'

interface IRequest {
	email: string
	password: string
}

interface IReturn {
	user: IUser
	token: string
}

export class SignInUserService {
	constructor(private readonly userRepository = UserRepository) {}

	public async execute({ email, password }: IRequest): Promise<IReturn> {
		const user = await this.userRepository.findByEmail(email)

		if (user === null) {
			throw new InternalAppError('User not found.')
		}

		const CheckUserPassword = await compare(password, user.password)

		if (CheckUserPassword === false) {
			throw new InternalAppError('Incorrect email/password combination.')
		}

		const token = sign({}, process.env.JWT_TOKEN as string, {
			subject: user.id,
			expiresIn: '1d'
		})

		return { user, token }
	}
}
