import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { UserRepository } from '../repositories/user.repository'
import { UserTokenRepository } from '../repositories/userToken.repository'

interface IRequest {
	email: string
}

export class ForgotPasswordService {
	constructor(
		private readonly userRepository = UserRepository,
		private readonly userTokenRepository = UserTokenRepository
	) {}

	public async execute({ email }: IRequest): Promise<void> {
		const user = await this.userRepository.findByEmail(email)

		if (user === null) {
			throw new InternalAppError('User not found.')
		}

		await this.userTokenRepository.generateToken(user.id)
	}
}
