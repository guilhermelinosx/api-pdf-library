import { hash } from 'bcryptjs'
import { addHours, isAfter } from 'date-fns'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { UserRepository } from '../repositories/user.repository'
import { UserTokenRepository } from '../repositories/userToken.repository'

interface IRequest {
	token: string
	password: string
}

export class ResetPasswordService {
	constructor(
		private readonly userRepository = UserRepository,
		private readonly userTokenRepository = UserTokenRepository
	) {}

	public async execute({ token, password }: IRequest): Promise<void> {
		const userToken = await this.userTokenRepository.findByToken(token)

		if (userToken === null) {
			throw new InternalAppError('User token does not exists.')
		}

		const user = await this.userRepository.findById(userToken.user_id)
		if (user === null) {
			throw new InternalAppError('User not found.')
		}

		const tokenCreatedAt = userToken.created_at

		const compareDate = addHours(tokenCreatedAt, 2)

		if (isAfter(Date.now(), compareDate)) {
			throw new InternalAppError('Token expired.')
		}

		user.password = await hash(password, 8)

		await this.userRepository.save(user)
	}
}
