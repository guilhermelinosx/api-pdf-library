import { hash } from 'bcryptjs'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { IUser } from '../models/IUser'
import { UserRepository } from '../repositories/user.repository'

export interface IRequest {
	name: string
	email: string
	password: string
}

export class SignUpUserService {
	constructor(private readonly userRepository = UserRepository) {}

	public async execute({ name, email, password }: IRequest): Promise<IUser> {
		const userCheck = await this.userRepository.findByEmail(email)
		if (userCheck !== null) {
			throw new InternalAppError('Email address already used.')
		}

		const user = this.userRepository.create({
			name,
			email,
			password: await hash(password, 8)
		})

		await this.userRepository.save(user)

		return user
	}
}
