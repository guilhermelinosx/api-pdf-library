import { database } from '../../../shared/database'
import { UserToken } from '../../../shared/database/entities/UserToken'

export const UserTokenRepository = database.getRepository(UserToken).extend({
	async findByToken(token: string): Promise<UserToken | null> {
		const userToken = await this.findOneBy({ token })
		return userToken
	},

	async generateToken(user_id: string): Promise<UserToken | null> {
		const userToken = this.create({ user_id })
		await this.save(userToken)
		return userToken
	}
})
