import { database } from '../../../shared/database'
import { User } from '../../../shared/database/entities/User'

export const UserRepository = database.getRepository(User).extend({
	async findById(id: string): Promise<User | null> {
		const user = await this.findOneBy({ id })
		return user
	},

	async findByName(name: string): Promise<User | null> {
		const user = await this.findOneBy({ name })
		return user
	},

	async findByEmail(email: string): Promise<User | null> {
		const user = await this.findOneBy({ email })
		return user
	}
})
