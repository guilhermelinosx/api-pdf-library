# <div align="center"> API PDF Library </div>

<div align="center">
<p>🚧 It is in Development 🚧</p>
</br>
<p></p>
</div>

## Technologies used in the project

- Typescript
- NodeJS
- Express
- TypeORM
- Jest
- PostgreSQL

## Technologies used outside the project

- Docker
- insominia
- Beekeeper

## How to run the project

- Clone this repository

```shell
git clone https://github.com/guilhermelinosx/api-sales.git
```

- Create a PostgreSQL Container using Docker

```shell
docker container create --name api-sales -e POSTGRES_PASSWORD=admin -p 5432:5432 postgres
```

- Start the Container

```shell
docker container start api-sales
```

- Stop the Container

```shell
docker container stop api-sales
```

- Start the Application in Development

```shell
yarn dev
```

- To stop the Application click CTRL+C in your terminal
